#pragma once
#include "base.h"
class ProcessScanner {
	std::wstring mProcessName = {};
	HANDLE mHProcess = nullptr;

public:
	DWORD PID = 0;
	DWORD_PTR mBaseAddress = 0;
	DWORD_PTR mEntryPoint = 0;
	std::wstring mExePath = {};

private:
	BOOL searchForProcess();
	BOOL searchForModule();

public:
	ProcessScanner(std::wstring processName)
		: mProcessName(processName)
	{
		if (searchForProcess())
		{
			LOGSCANNER(L"Failed to locate process \"" << mProcessName << L"\"!");
		}
		else {
			if (searchForModule())
			{
				LOGSCANNER(L"Failed enumerating process modules!");
			}
			else {
				LOGSCANNER(L"Found process \"" << mProcessName << L"\"!");
				std::wcout << L"  PID: " << PID << std::endl;
				std::wcout << L"  Exe Path: " << mExePath << std::endl;
				std::wcout << L"  Base Address: ";
				PRINTPTR(mBaseAddress);
				std::wcout << L"  Entry Point: ";
				PRINTPTR(mEntryPoint);
			}
		}
		
	};
};