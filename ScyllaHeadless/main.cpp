#include "base.h"
#include "ProcessScanner.h"

HMODULE mod = LoadLibraryA("Scylla_x64.dll");

typedef BOOL(__stdcall* def_ScyllaDumpProcessW)(DWORD_PTR pid, const WCHAR* fileToDump, DWORD_PTR imagebase, DWORD_PTR entrypoint, const WCHAR* fileResult);
def_ScyllaDumpProcessW ScyllaDumpProcessW = (def_ScyllaDumpProcessW)GetProcAddress(mod, "ScyllaDumpProcessW");

int main(int argc, char* argv[])
{
    if (argc != 3)
    {
        std::wcout << L"Invalid Arguments! Requires: \"Process Name\" \"Dump File Path\"" << std::endl;
        return 1;
    }

    //hello my terrible cstyle str conversions
    size_t processStrSize = strlen(argv[1]) + 1;
    wchar_t* process = new wchar_t[processStrSize];
    size_t processOutSize;
    mbstowcs_s(&processOutSize, process, processStrSize, argv[1], processStrSize - 1);

    size_t pathStrSize = strlen(argv[2]) + 1;
    wchar_t* path = new wchar_t[pathStrSize];
    size_t pathOutSize;
    mbstowcs_s(&pathOutSize, path, pathStrSize, argv[2], pathStrSize - 1);

    ProcessScanner scanner(process);
    ScyllaDumpProcessW(scanner.PID, scanner.mExePath.c_str(), scanner.mBaseAddress, scanner.mEntryPoint, path);

    std::wcout << "[Scylla Dump] Process Dumped Sucessfully!\nSaved to: " << path << std::endl;
    return 0;
}
