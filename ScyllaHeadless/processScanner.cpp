#include "processScanner.h"
BOOL ProcessScanner::searchForProcess()
{
	PROCESSENTRY32 entry;
	entry.dwSize = sizeof(PROCESSENTRY32);
	HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);
	if (Process32First(snapshot, &entry) == TRUE)
	{
		//Traverse Executing Processes
		do {
			if (wcscmp(entry.szExeFile, mProcessName.c_str()) == 0)
			{
				PID = entry.th32ProcessID;
				mHProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, PID);
				CloseHandle(snapshot);
				return 0;
			}

		} while (Process32Next(snapshot, &entry) == TRUE);
	}
	CloseHandle(snapshot);
	return 1;
}

BOOL ProcessScanner::searchForModule()
{
	MODULEENTRY32 mEntry;
	mEntry.dwSize = sizeof(MODULEENTRY32);
	HANDLE mSnap = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, PID);
	if (Module32First(mSnap, &mEntry) == TRUE)
	{
		//Traverse Module List
		do {
			if (wcscmp(mEntry.szModule, mProcessName.c_str()) == 0)
			{
				MODULEINFO mInfo;
				if (GetModuleInformation(mHProcess, mEntry.hModule, &mInfo, sizeof(MODULEINFO)) == TRUE)
				{
					mBaseAddress = (DWORD_PTR)mEntry.modBaseAddr;
					mEntryPoint = (DWORD_PTR)mInfo.EntryPoint;
					mExePath = mEntry.szExePath;
					CloseHandle(mHProcess);
					CloseHandle(mSnap);
					return 0;
				}
			}
		} while (Module32Next(mSnap, &mEntry) == TRUE);
	}
	CloseHandle(mHProcess);
	CloseHandle(mSnap);
	return 1;
}
