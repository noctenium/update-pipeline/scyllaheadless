#pragma once
#include <Windows.h>
#include <iostream>
#include <iomanip>
#include <tlhelp32.h>
#include <Psapi.h>

#define LOGSCANNER(msg) std::wcout << L"[Scanner] " << msg << std::endl;
#define PRINTPTR(ptr) std::wcout << L"0x" << std::setfill(L'0') << std::setw(16) << std::right << ptr << std::hex << std::endl;