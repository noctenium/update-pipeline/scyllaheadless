## ScyllaHeadless
Allows you to dump a process' memory using [Scylla](https://github.com/NtQuery/Scylla) via it's included DLL allowing for scripting capability

## Usage
`./ScyllaHeadless.exe [processName] [dumpFilePath]`

| Argument | Description |
| --- | --- |
| processName | the name of the executing process you want to dump (ex. `process.exe`) |
| dumpFilePath | the absolute path of where you want the dump file and what you want it called (ex. `C:\\process_dump.exe`) |
